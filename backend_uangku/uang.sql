-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2017 at 03:13 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `uang`
--

-- --------------------------------------------------------

--
-- Table structure for table `uang_keluar`
--

CREATE TABLE IF NOT EXISTS `uang_keluar` (
  `id_keluar` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `kategori` enum('Belanja','Konsumsi','Kesehatan','Transportasi','Lain-lain','') NOT NULL,
  `catatan` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uang_keluar`
--

INSERT INTO `uang_keluar` (`id_keluar`, `id_user`, `nominal`, `kategori`, `catatan`, `tanggal`, `jam`) VALUES
(0, 3, 10000, 'Transportasi', 'sjadhjsdskjd', '2017-05-10', '00:00:00'),
(0, 3, 9000, 'Kesehatan', 'jsdhksahdkasj', '2017-05-16', '00:00:00'),
(0, 3, 8000, 'Konsumsi', 'vvvv', '2017-05-17', '00:00:00'),
(0, 3, 8000, 'Konsumsi', 'vvvv', '2017-05-17', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `uang_masuk`
--

CREATE TABLE IF NOT EXISTS `uang_masuk` (
  `id_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `kategori` enum('Gaji','Laba','Bonus','Lain-lain') NOT NULL,
  `catatan` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  PRIMARY KEY (`id_masuk`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `uang_masuk`
--

INSERT INTO `uang_masuk` (`id_masuk`, `id_user`, `nominal`, `kategori`, `catatan`, `tanggal`, `jam`) VALUES
(1, 3, 200000, 'Gaji', 'xxxxxxxxxxxxxxxxx', '2017-05-16', '00:00:00'),
(2, 3, 300000, 'Laba', 'vvvvvvvvvvvvvvvvvvvvvvv', '2017-05-24', '00:00:00'),
(3, 3, 10000, 'Gaji', 'shsh', '2001-08-23', '10:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`) VALUES
(3, 'paijo', 'paijo', '202cb962ac59075b964b07152d234b70');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `uang_keluar`
--
ALTER TABLE `uang_keluar`
  ADD CONSTRAINT `uang_keluar_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `uang_masuk`
--
ALTER TABLE `uang_masuk`
  ADD CONSTRAINT `uang_masuk_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: *");
	define('DB_SERVER', 'localhost');
	define('DB_USER','root');
	define('DB_PASS','');
	define('DB_NAME','uangku');
	class AksesDB{
		var $konek;
		function __construct(){
			$this->konek = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME) OR die("Gagal Koneksi");
		}
		function ambilData($id,$tabel){
			$query=mysqli_query($this->konek,"select * from {$tabel} where id_user='{$id}'  order by tanggal desc");
			return $query;
		}
		
		function tambahData($tabel,$id,$nominal,$kategori,$catatan,$tanggal,$jam){
			$query=mysqli_query($this->konek,"INSERT INTO `$tabel` (`id_user`, `nominal`, `kategori`, `catatan`, `tanggal`, `jam`) VALUES ($id, $nominal, '$kategori', '$catatan', '$tanggal', '$jam')");
			return $query;
				
		}

		function updateData($tabel,$id,$nominal,$kategori,$catatan,$tanggal,$jam){
			$query=mysqli_query($this->konek,"UPDATE `$tabel` SET `nominal` = '$nominal', `kategori` = '$kategori', `catatan` = '$catatan', `tanggal` = '$tanggal', `jam` = '$jam' WHERE `$tabel`.`id` = $id");
			return $query;
				
		}

		function ambilTotal($id,$tabel){
			$query=mysqli_query($this->konek,"select * from {$tabel} where id_user='{$id}'");
			return $query;
		}
		function hapus($id,$tabel){
			$query=mysqli_query($this->konek,"delete from {$tabel} where id={$id}");
			return $query;
		}

		function ambilUang($id,$tabel){
			$query=mysqli_query($this->konek,"select * from {$tabel} where id='{$id}'");
			return $query;
		}
	}
	$method = $_GET['method'];
	if ($method =='ambil'){
		$id = $_GET['id'];
		$tabel = $_GET['tabel'];
		
		$ambil=new AksesDB;
		$hasil=$ambil->ambilData($id,$tabel);	
		$json1="[";
		while ($row=mysqli_fetch_assoc($hasil)){
			if($json1!=="["){$json1.=", ";}
			$json1 .= json_encode($row);
		}
		$json1.="]";
		echo $json1;
	}else if($method =='tambah'){
		$id = $_GET['id'];
		$tabel = $_GET['tabel'];
		$nominal = $_GET['nominal'];
		$kategori = $_GET['kategori'];
		$catatan = $_GET['catatan'];
		$tanggal = $_GET['tanggal'];
		$jam = $_GET['jam'];
		
		$tambah=new AksesDB;
		$hasil=$tambah->tambahData($tabel,$id,$nominal,$kategori,$catatan,$tanggal,$jam);
		if($hasil)
			echo '[{"status":"sukses"}]';
		else 
			echo '[{"status":"gagal"}]';
	}else if ($method =='ambiltotal'){
		$tabel = $_GET['tabel'];
		$id = $_GET['id'];
		$total = 0;
		$ambil=new AksesDB;
		$hasil=$ambil->ambiltotal($id,$tabel);	
		while ($row=mysqli_fetch_assoc($hasil)){
			$total = $total+$row['nominal']; 
		}
		$json = '[{"total":'.$total.'}]';
		echo $json;
	}else if ($method =='hapus'){
		$tabel = $_GET['tabel'];
		$id = $_GET['id'];
		$total = 0;
		$ambil=new AksesDB;
		$hasil=$ambil->hapus($id,$tabel);	
		if ($hasil)
			echo '[{"status":"sukses"}]';
		else 
			echo '[{"status":"gagal"}]';
	}else if($method =='edit'){
		$id = $_GET['id'];
		$tabel = $_GET['tabel'];
		$nominal = $_GET['nominal'];
		$kategori = $_GET['kategori'];
		$catatan = $_GET['catatan'];
		$tanggal = $_GET['tanggal'];
		$jam = $_GET['jam'];
		
		$tambah=new AksesDB;
		$hasil=$tambah->updateData($tabel,$id,$nominal,$kategori,$catatan,$tanggal,$jam);
		if($hasil)
			echo '[{"status":"sukses"}]';
		else 
			echo '[{"status":"gagal"}]';
	}else if ($method =='ambiluang'){
		$id = $_GET['id'];
		$tabel = $_GET['tabel'];
		
		$ambil=new AksesDB;
		$hasil=$ambil->ambilUang($id,$tabel);	
		$json1="[";
		while ($row=mysqli_fetch_assoc($hasil)){
			if($json1!=="["){$json1.=", ";}
			$json1 .= json_encode($row);
		}
		$json1.="]";
		echo $json1;
	}else if($method=='selisih'){
		$id = $_GET['id'];
		$selisih = 0;
		$pendapatan = 0;
		$pengeluaran = 0;

		$ambil=new AksesDB;
		$hasil=$ambil->ambiltotal($id,'uang_masuk');	
		while ($row=mysqli_fetch_assoc($hasil)){
			$pendapatan = $pendapatan+$row['nominal']; 
		}
		$ambil=new AksesDB;
		$hasil=$ambil->ambiltotal($id,'uang_keluar');	
		while ($row=mysqli_fetch_assoc($hasil)){
			$pengeluaran = $pengeluaran+$row['nominal']; 
		}
		$selisih = $pendapatan - $pengeluaran; 
		echo '[{"selisih":'.$selisih.'}]';
	}
?>
